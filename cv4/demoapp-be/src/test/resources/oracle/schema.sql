-- 1] =============== CREATE SCHEMA ===============

-- login under system

alter session set "_ORACLE_SCRIPT"=true;
-- 1] =============== CREATE SCHEMA ===============

-- USER SQL
CREATE USER "DIGIDBD_DAPBE" IDENTIFIED BY "password"
    DEFAULT TABLESPACE "USERS"
    TEMPORARY TABLESPACE "TEMP";

-- QUOTAS
ALTER USER "DIGIDBD_DAPBE" QUOTA UNLIMITED ON "USERS";

-- ROLES
GRANT "CONNECT" TO "DIGIDBD_DAPBE" ;
GRANT "RESOURCE" TO "DIGIDBD_DAPBE" ;
ALTER USER "DIGIDBD_DAPBE" DEFAULT ROLE "CONNECT","RESOURCE";

-- SYSTEM PRIVILEGES
GRANT CREATE VIEW TO "DIGIDBD_DAPBE" ;
GRANT CREATE TABLE TO "DIGIDBD_DAPBE" ;
GRANT CREATE SEQUENCE TO "DIGIDBD_DAPBE" ;
GRANT UNLIMITED TABLESPACE TO "DIGIDBD_DAPBE" ;
GRANT CREATE PROCEDURE TO "DIGIDBD_DAPBE" ;