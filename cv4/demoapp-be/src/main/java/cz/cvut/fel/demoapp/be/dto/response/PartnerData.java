package cz.cvut.fel.demoapp.be.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PartnerData {

    private Long id;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;

}
