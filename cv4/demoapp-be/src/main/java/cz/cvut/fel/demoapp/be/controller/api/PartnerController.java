package cz.cvut.fel.demoapp.be.controller.api;

import cz.cvut.fel.demoapp.be.model.Partner;
import cz.cvut.fel.demoapp.be.service.PartnerDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PartnerController {

    @Autowired
    private PartnerDataService partnerDataService;

    @GetMapping("/partners/{partnerId}")
    public Partner getPartnerData(@PathVariable Long partnerId) {
        return partnerDataService.getPartnerById(partnerId);
    }

    @GetMapping("/partners")
    public Partner getPartnerDataList(@RequestParam(required = false) String firstName,
                                      @RequestParam(required = false) String lastName,
                                      @RequestParam(required = false) String dateOfBirth) {
        return partnerDataService.getFirstByFirstNameOrLastNameOrDateOfBirth(firstName, lastName, dateOfBirth);
    }
}
