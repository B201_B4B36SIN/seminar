package cz.cvut.fel.demoapp.be.model;

import lombok.Getter;

@Getter
public enum CbStatus implements AbstractCodeBook {

    ACTIVE("Active"),
    COMPLETED("Completed");

    private final String description;

    CbStatus(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return null;
    }
}
