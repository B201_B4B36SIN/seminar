package cz.cvut.fel.demoapp.be.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerSearch {

    private Long id;
    private String firstName;
    private String lastName;
    private String dateOfBirth;

}
