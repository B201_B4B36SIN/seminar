package cz.cvut.fel.demoapp.be.model;

public interface AbstractCodeBook {
    String getDescription();
    int ordinal();
}
