package cz.cvut.fel.demoapp.be.config;

import cz.cvut.fel.demoapp.be.model.ModelPackage;
import cz.cvut.fel.demoapp.be.repository.RepositoryPackage;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.time.Clock;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.Properties;

@Configuration
@EnableJpaAuditing(dateTimeProviderRef = JpaConfig.AUDITING_DATE_TIME_PROVIDER_COMPONENT_NAME)
@EnableJpaRepositories(basePackageClasses = RepositoryPackage.class)
public class JpaConfig {

    public static final String AUDITING_DATE_TIME_PROVIDER_COMPONENT_NAME = "auditingDateTimeProvider";

    @Value("${spring.jpa.database-platform}")
    private String hibernateDialect;

    @Bean
    public Clock systemDefaultZoneClock() {
        return Clock.systemDefaultZone();
    }

    @Bean(name = AUDITING_DATE_TIME_PROVIDER_COMPONENT_NAME)
    public DateTimeProvider dateTimeProvider(Clock clock) {
        return () -> Optional.of(OffsetDateTime.now(clock));
    }

    @Bean
    public Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");
        properties.setProperty("hibernate.dialect", hibernateDialect);
        return properties;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory(DataSource dataSource, Properties jpaProperties) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(ModelPackage.class.getPackage().getName());
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        entityManagerFactoryBean.afterPropertiesSet();
        return entityManagerFactoryBean.getObject();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory exposureEntityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(exposureEntityManagerFactory);
        return transactionManager;
    }

    @Bean
    public FlywayMigrationStrategy flywayMigrationStrategy() {
        return Flyway::migrate;
    }
}