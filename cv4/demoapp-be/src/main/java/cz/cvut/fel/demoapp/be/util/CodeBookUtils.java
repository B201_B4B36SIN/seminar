package cz.cvut.fel.demoapp.be.util;

import cz.cvut.fel.demoapp.be.exception.FieldInvalidException;

public class CodeBookUtils {

    public static <T extends Enum> T cb(Class<T> cbStatusClass, String code) {
        try {
            return (T) Enum.valueOf(cbStatusClass, code);
        } catch (Exception e) {
            throw new FieldInvalidException();
        }
    }
}
