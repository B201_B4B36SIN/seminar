package cz.cvut.fel.demoapp.be.service;

import cz.cvut.fel.demoapp.be.dto.response.CodeBookItem;
import cz.cvut.fel.demoapp.be.model.AbstractCodeBook;
import org.springframework.stereotype.Component;

@Component
public class CodeBookMapper {

    public CodeBookItem map(AbstractCodeBook cb) {
        CodeBookItem dto = new CodeBookItem();
        dto.setCode(cb.toString());
        dto.setDescription(cb.getDescription());
        dto.setOrder(cb.ordinal());
        return dto;
    }
}
