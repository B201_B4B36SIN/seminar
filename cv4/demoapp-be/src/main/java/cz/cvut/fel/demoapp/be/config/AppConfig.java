package cz.cvut.fel.demoapp.be.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({SwaggerConfig.class, JpaConfig.class})
public class AppConfig {
}
