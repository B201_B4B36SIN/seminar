package cz.cvut.fel.demoapp.be.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "TASK")
public class Task extends AbstractEntity {

    @Column(name = "PARTNER_ID")
    private String partnerId;

    @Column(name = "STATUS_CODE")
    @Enumerated(EnumType.STRING)
    private CbStatus status;

    @Column(name = "TEXT")
    private String text;
}
