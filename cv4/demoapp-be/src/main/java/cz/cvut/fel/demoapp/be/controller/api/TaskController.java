package cz.cvut.fel.demoapp.be.controller.api;

import cz.cvut.fel.demoapp.be.dto.request.TaskCreate;
import cz.cvut.fel.demoapp.be.dto.request.TaskUpdate;
import cz.cvut.fel.demoapp.be.dto.request.TaskUpdateStatus;
import cz.cvut.fel.demoapp.be.dto.response.Task;
import cz.cvut.fel.demoapp.be.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/partners/{partnerId}")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public List<Task> findByPartnerId(@PathVariable(required = false) String partnerId) {
        return taskService.findByPartnerId(partnerId);
    }

    @PostMapping("/tasks")
    public long createTask(@PathVariable String partnerId,
                           @RequestBody @Valid TaskCreate task) {
        return taskService.create(partnerId, task);
    }

    @PostMapping("/delete-completed-tasks")
    public ResponseEntity<Object> deleteCompletedTasks(@PathVariable String partnerId) {
        taskService.deleteCompletedTasks(partnerId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/tasks/{taskId}")
    public ResponseEntity<Object> update(@PathVariable String partnerId,
                                         @PathVariable long taskId,
                                         @RequestBody TaskUpdate task) {
        taskService.update(partnerId, taskId, task);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/tasks/{taskId}")
    public ResponseEntity<Object> delete(@PathVariable String partnerId,
                                         @PathVariable long taskId) {
        taskService.delete(partnerId, taskId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/tasks/{taskId}")
    public ResponseEntity<Object> updateStatus(@PathVariable String partnerId,
                                               @PathVariable long taskId,
                                               @RequestBody @Valid TaskUpdateStatus request) {
        taskService.updateStatus(partnerId, taskId, request);
        return ResponseEntity.noContent().build();
    }
}
