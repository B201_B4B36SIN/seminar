package cz.cvut.fel.demoapp.be.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class CodeBookItemIdentifier {

    @NotEmpty
    private String code;

}
