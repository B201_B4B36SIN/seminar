package cz.cvut.fel.demoapp.be.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class CodeBookItem {

    private String code;
    private String description;
    private int order;
    private Map<String, Object> attributes = new HashMap<>();

}
