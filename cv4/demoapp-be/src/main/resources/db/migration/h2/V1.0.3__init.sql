drop sequence DEMOAPP_SEQUENCE;
drop table TASK;

create sequence DEMOAPP_SEQUENCE start with 1 increment by 1;

create table TASK (
    ID                   number(10) unique not null,
    CREATED_DATE         datetime,
    LAST_MODIFIED_DATE   datetime,
    VERSION              number(10),
    PARTNER_ID           varchar(50 char),
    STATUS_CODE          varchar(50 char),
    TEXT                 varchar(2000 char)
);

create table PARTNER (
    ID                   number(10) unique not null,
    CREATED_DATE         datetime,
    LAST_MODIFIED_DATE   datetime,
    VERSION              number(10),
    FIRSTNAME           varchar(50 char),
    LASTNAME            varchar(50 char),
    PERSONNUMBER        number(10)
);

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0,
        '111', 'ACTIVE', 'Identify resources to be monitored');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0,
        '111', 'ACTIVE', 'Define users and workflow');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0,
        '111', 'ACTIVE', 'Identify event sources by resource type');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0,
        '222', 'COMPLETED', 'Define the relationship between resources and business systems');

insert into Partner (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, FIRSTNAME, LASTNAME, PERSONNUMBER)
values (111, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0,
        '111', '111', '111');
