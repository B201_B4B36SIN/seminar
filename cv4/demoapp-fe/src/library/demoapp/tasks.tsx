export type PartnerTaskItem = {
    id: number,
    text: string,
    status: {
        code: "ACTIVE" | "COMPLETED",
        description: string,
        order: number,
        attributes: any
    }
};
