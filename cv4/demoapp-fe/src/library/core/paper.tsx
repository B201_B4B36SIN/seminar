export type CEZPaperItem = {
    component: React.Component,
    title?: string,
};
