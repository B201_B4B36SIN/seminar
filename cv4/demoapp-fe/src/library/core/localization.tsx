export enum ELocalizationLanguage {
    CZ = "CZ",
    ENGB = "ENGB"
};

export type LanguageSelectProps = {
    language: ELocalizationLanguage,
    setLanguage: (lang: ELocalizationLanguage) => void
};

export type LocalizationPayload = ELocalizationLanguage;
