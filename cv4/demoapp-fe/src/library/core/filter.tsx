export interface CEZFilterPanelProps<T> {
    onFilter: (searchQuery: string, filterType: any) => void
    options: CEZFilterBodyItem<T>[]
};

export interface CEZFilterBodyItem<T> {
    value: T,
    text: string
};
