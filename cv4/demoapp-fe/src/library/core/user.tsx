export type UserContextItem = {
    name: string,
    surname: string,
    isLoggedIn: boolean,
    redirectToLogin: boolean
};
