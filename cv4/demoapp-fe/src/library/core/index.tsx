export * from "./filter";
export * from "./localization";
export * from "./paper";
export * from "./table";
export * from "./user";

export interface StoreAction<T> {
    type: string,
    payload: T
};
