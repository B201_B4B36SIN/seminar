import { DefaultRootState } from "react-redux";
import { PartnerDataItem } from "@library";

export type RootStoreState = DefaultRootState & {
    demoapp: {
        partners: PartnerDataItem[]
    }
};
