import { useContext } from "react";
import { UserContext } from "@core";

const useUser = () => {

    const userContext = useContext(UserContext);

    return {
        ...userContext
    }
};

export default useUser;
