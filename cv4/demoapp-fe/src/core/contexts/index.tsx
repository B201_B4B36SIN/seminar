import { createContext } from "react";
import { ELocalizationLanguage } from "@library";

export const LanguageContext = createContext({
    language: ELocalizationLanguage.CZ,
    setLanguage: (lang: ELocalizationLanguage) => {}
});

export const LoadingContext = createContext({
    inProgress: false,
    set: (on: boolean) => {}
});

export const UserContext = createContext({
    name: "",
    surname: "",
    isLoggedIn: true,
    redirectToLogin: false
});
