import SearchForPartners from "./SearchForPartners/SearchForPartners";
import PartnerDetailsPage from "./PartnerDetailsPage/PartnerDetailsPage";
import { DEMOAPP_URLS } from "@constants";

const routes = {
    searchForPartners: {
        path: DEMOAPP_URLS.partnersList,
        component: SearchForPartners
    },
    partnerDetails: {
        path: DEMOAPP_URLS.partnerDetails,
        component: PartnerDetailsPage
    }
};

export default routes;
