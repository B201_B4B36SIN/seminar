import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { Table } from "@components";
import { RootStoreState, PartnerDataItem } from "@library";
import { DEMOAPP_URLS } from "@constants";
import { useLocalization } from "@localization";
import { IconButton, Paper } from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import transFile from "./translations.json";
import PartnerFilters from "./PartnerFilters/PartnerFilters";

const SearchForPartners = () => {

    const partnersList = useSelector((state: RootStoreState) => state.demoapp.partners);
    const dispatch = useDispatch();
    const history = useHistory();
    const [partnersTableItems, setPartnersTableItems] = useState<any>();
    const translations = useLocalization(transFile);
   
    useEffect(() => {

        if (!partnersList) return;
    
        setPartnersTableItems(Object.keys(partnersList).map((itemKey: any) => {

            const partner: PartnerDataItem = partnersList[itemKey];
            const partnerIdUrl = DEMOAPP_URLS.partnerDetails.replace(":partnerId", partner.id?.toString());

            return {
                ...partner,
                goToPartner: (
                    <Link to={partnerIdUrl}>
                        <IconButton 
                            aria-label="Edit partner" 
                            data-test-id={`edit-partner-${partner.id}`}
                        >
                            <Edit />
                        </IconButton>
                    </Link>
                )
            };
        }));

    }, [partnersList, dispatch, history]);

    return (
        <>
            <PartnerFilters />
            <Paper>
                <Table
                    titles={[
                        { id: "firstName", value: translations.tableTitle1 },
                        { id: "lastName", value: translations.tableTitle2 },
                        { id: "dateOfBirth", value: translations.tableTitle3 },
                        { id: "goToPartner", value: "", unsortable: true }
                    ]}
                    mapItemsToTitleIds={true}
                    items={partnersTableItems}
                    emptyTableMessage={translations.emptyListMessage}
                />
            </Paper>
        </>
    );
};

export default SearchForPartners;
