import { useState, useContext } from "react";
import { EPartnerFilter } from "@library";
import * as api from "@api";
import { useDispatch } from "react-redux";
import { setPartners } from "@store";
import { LoadingContext } from "@core";

const useFilters = () => {
    
    const [activeFilter, setActiveFilter] = useState(EPartnerFilter.NAME_SURNAME_DOB);
    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");
    const [id, setId] = useState<number>(0);
    const [filterIsValid, setFilterIsValid] = useState<boolean>(false);
    const loading = useContext(LoadingContext);
    const dispatch = useDispatch();

    const resetFilters = () => {
        setName("");
        setSurname("");
        setDateOfBirth("");
        setId(0);
    };

    const setFilter = (type: string, value: any) => {

        const nameFilled = name.length > 0;
        const surnameFilled = surname.length > 0;
        const dateOfBirthFilled = dateOfBirth.length > 0;

        const nameOrSurname = nameFilled || surnameFilled;
        const nameOrDob = nameFilled || dateOfBirthFilled;
        const surnameOrDobValid = surnameFilled || dateOfBirthFilled;

        switch(type) {
            case "id": {
                if (value)
                setId(value);
                setFilterIsValid(value !== 0);
                break;
            } 
            case "name": {
                setName(value);
                setFilterIsValid(value.length > 0 && surnameOrDobValid);
                break;
            }
            case "surname": {
                setSurname(value);
                setFilterIsValid(value.length > 0 && nameOrDob);
                break;
            }
            case "dateOfBirth": {
                setDateOfBirth(value);
                setFilterIsValid(value.length > 0 && nameOrSurname);
                break;
            }
        }
    };

    const handleApplyFilters = () => {

        if (!filterIsValid) {
            throw new Error("'Apply filters' button should be disabled.")
        }

        loading.set(true);

        switch(activeFilter) {
            case EPartnerFilter.ID: {
                api.getPartnerById(id)
                .then(response => {

                    dispatch(setPartners([response.data]));
                    loading.set(false);

                })
                .catch(error => {
                    loading.set(false);
                });
                break;
            }
            default: {
                api.getFilteredPartners(name, surname, dateOfBirth)
                .then(response => {

                    dispatch(setPartners(response.data));
                    loading.set(false);

                })
                .catch(error => {
                    loading.set(false);
                });
            }
        }
    };

    const handleFilterChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        resetFilters();
        setActiveFilter(event.target.value as EPartnerFilter);
    };

    return {
        activeFilter,
        name,
        surname,
        dateOfBirth,
        id,
        set: setFilter,
        reset: resetFilters,
        setActive: setActiveFilter,
        handleApplyFilters: handleApplyFilters,
        handleFilterChange,
        idFilterValid: activeFilter === EPartnerFilter.ID && id !== 0,
        filterIsInvalid: !filterIsValid,
        invalidNameTooLong: name.length > 40,
        invalidSurnameTooLong: surname.length > 40,
        invalidIdTooLong: id.toString().length > 19
    };
}

export default useFilters;
