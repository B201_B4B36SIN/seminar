import { useState } from "react";
import * as api from "@api";
import { PartnerTaskItem } from "@library";

const useTasksList = () => {

    const [tasks, setTasks] = useState<PartnerTaskItem[]>([]);
    const [hasCompletedTasks, setHasCompletedTasks] = useState<boolean>(false);
    const [partnerId, setPartnerId] = useState<number>();

    const onPartnerUpdated = (partnerId: number) => {
        
        setPartnerId(partnerId);

        api.getTasks(partnerId)
        .then(response => {

            const hasCompletedItems = response.data.some((task: PartnerTaskItem) => 
                task.status.code === "COMPLETED"
            );

            setHasCompletedTasks(hasCompletedItems);
            setTasks(response.data);
            
        })
        .catch(error => setTasks([]));
    };

    const toggleTaskCompleted = (taskId: number) => {

        const task = tasks.find(task => task.id === taskId);
        const setTaskToActive = task.status.code === "COMPLETED";
        let hasIncompleteTasks = false;

        api.updateTaskState(partnerId, taskId, setTaskToActive).then(() => {
            setTasks(tasks.map((item: PartnerTaskItem) => {

                if (item.id === taskId) {
                    item.status.code = setTaskToActive ? "ACTIVE" : "COMPLETED";
                }

                if (item.status.code === "COMPLETED") {
                    hasIncompleteTasks = true;
                }

                return item;
                
            }));

            setHasCompletedTasks(hasIncompleteTasks);
        });
        
    };

    const deleteCompletedTasks = () => {

        api.deleteCompletedTasks(partnerId)
        .then(() => {

            setTasks(tasks.filter((item: PartnerTaskItem) => item.status.code !== "COMPLETED"));
            setHasCompletedTasks(false);

        });
    };

    const addNewTask = (newTaskText: string) => {
        
        api.createNewTask(partnerId, newTaskText)
        .then(response => {

            const newTask: PartnerTaskItem = {
                id: response.data,
                text: newTaskText,
                status: {
                    code: "ACTIVE",
                    description: "New task",
                    order: -1,
                    attributes: {}
                }
            }

            setTasks([...tasks, newTask]);
        })
    };
    
    const deleteTask = (taskId: number) => {

        api.deleteTask(partnerId, taskId).then(() => {

            let hasCompletedItems = false;

            const newTasks = tasks.filter((task: PartnerTaskItem) => {
                
                if (task.id !== taskId && task.status.code === "COMPLETED") {
                    hasCompletedItems = true;
                }

                return task.id !== taskId;
            });

            setHasCompletedTasks(hasCompletedItems);
            setTasks(newTasks);

        });
    };

    const onTaskEdited = (editedTask: PartnerTaskItem) => {
        setTasks(tasks.map(task => {
            if (task.id === editedTask.id) task = editedTask;
            return task;
        }))
    };

    return {
        onPartnerUpdated,
        toggleTaskCompleted,
        deleteCompletedTasks,
        deleteTask,
        addNewTask,
        items: tasks,
        onTaskEdited,
        set: setTasks,
        hasFinishedTasks: hasCompletedTasks
    };
};  

export default useTasksList;
