import React, { useState, useEffect, useContext } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Typography } from "@material-ui/core";
import { useLocalization } from "@localization";
import { DEMOAPP_URLS } from "@constants";
import { ModalProgress } from "@components";
import { PartnerDataItem } from "@library";
import * as api from "@api";
import { LoadingContext } from "@core";
import TasksList from "./TasksList/TasksList";
import PartnerDetails from "./PartnerDetails/PartnerDetails";
import transFile from "./translations.json";

const PartnerDetailsPage = () => {

    const history = useHistory();
    const [partner, setPartner] = useState<PartnerDataItem>();
    const loading = useContext(LoadingContext);
    const translations = useLocalization(transFile);
    
    const { partnerId } = useParams();

    useEffect(() => {
        if (!partnerId) {
            history.push(DEMOAPP_URLS.partnersList);
        }
    }, [partnerId, history]);

    useEffect(() => {

        loading.set(true);
        api.getPartnerById(partnerId)
        .then(response => {
            setPartner(response.data);
            loading.set(false);
        })
        .catch(error => {
            if (error.response.status === 404) {
                loading.set(false);
            }
        });
        
    }, [partnerId]);

    return (
        <>
            <Typography>{translations.title}</Typography>
            {partner &&
                <>
                    <PartnerDetails partner={partner}/>
                    <TasksList partner={partner}/>
                </> 
            }
            {!partner &&
                <Typography color="error">
                    {translations.errorPartnerIdTaskFetch.replace(":id", `id: ${partnerId}`)}
                </Typography>
            }

            <ModalProgress open={loading.inProgress} />
        </>
    );
};

export default PartnerDetailsPage;
