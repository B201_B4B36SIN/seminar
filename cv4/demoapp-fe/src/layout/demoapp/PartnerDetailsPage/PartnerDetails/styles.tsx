import { makeStyles, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        paper: {
            display: "flex",
            flexWrap: "wrap"
        },
        container: {
            padding: "1rem 0",
            width: "50%",
            "&:nth-child(even)": {
                textAlign: "end"
            }
        }
    })
);

export default useStyles;
