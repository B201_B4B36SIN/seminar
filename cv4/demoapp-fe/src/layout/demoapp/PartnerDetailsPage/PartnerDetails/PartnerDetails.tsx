import React from "react";
import { Typography, Container, Paper } from "@material-ui/core";
import transFile from "./translations.json";
import { useLocalization } from "@localization";
import { PartnerDetailsProps } from "@library";
import useStyles from "./styles";

const PartnerDetails = ({ partner }: PartnerDetailsProps) => {

    const translations = useLocalization(transFile);
    const styles = useStyles();

    return (
        <Paper className={styles.paper}>
            <Container className={styles.container}>
                <Typography color="textSecondary">{translations.nameAndSurname}</Typography>
                <Typography>{`${partner.lastName}, ${partner.firstName}`}</Typography>
            </Container>
            <Container className={styles.container}>
                <Typography color="textSecondary">{translations.dateOfBirth}</Typography>
                <Typography>{partner.dateOfBirth}</Typography>
            </Container>
            <Container className={styles.container}>
                <Typography color="textSecondary">{translations.partnerId}</Typography>
                <Typography>{partner.id}</Typography>
            </Container>
            <Container className={styles.container}>
                <Typography color="textSecondary">{translations.birthNumber}</Typography>
                <Typography>{partner.personNumber}</Typography>
            </Container>
        </Paper>
    );
};

export default PartnerDetails;
