import React, { useState } from "react";
import { Button } from "@components";
import { Typography, Modal, TextField, Grid, Paper } from "@material-ui/core";
import { ListAlt, FindInPage, RecentActors } from "@material-ui/icons";
import { useHistory, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { DEMOAPP_URLS } from "@constants";
import * as api from "@api";
import { useLocalization } from "@localization";
import useStyles, { useMenuButtonStyles } from "./styles";
import transFile from "./translations.json";

const MainPage = () => {

    const [isPartnerTasksModalOpen, setPartnerTasksModalOpen] = useState(false);
    const [partnerId, setPartnerId] = useState<number>(0);
    const history = useHistory();
    const styles = useStyles();
    const menuButtonStyles = useMenuButtonStyles();
    const translations = useLocalization(transFile);

    const handleOpenTasksModal = () => {
        setPartnerTasksModalOpen(true)
    };
    
    return (
        <>
            <Grid container justify="center" spacing={2}>
                <Grid item>
                    <Button
                        classes={menuButtonStyles}
                        aria-label="go to search partners"
                        data-test-id="go-to-search-partners"
                        variant="outlined"
                        startIcon={<ListAlt />}
                        onClick={e => history.push(DEMOAPP_URLS.partnersList)}
                    >{translations.viewPartnersLabel}</Button>
                </Grid>
                <Grid item>
                    <Button
                        classes={menuButtonStyles}
                        aria-label="open partner details dialog"
                        data-test-id="partner-details-modal-button"
                        variant="outlined"
                        startIcon={<FindInPage />}
                        onClick={handleOpenTasksModal}
                    >{translations.viewNewTasksLabel}</Button>
                </Grid>
                <Grid item>
                    <Link to={DEMOAPP_URLS.partnerDetails.replace(":partnerId", (2366739).toString())}>
                        <Button
                            classes={menuButtonStyles}
                            aria-label="test"
                            data-test-id="test-id"
                            variant="outlined"
                            startIcon={<RecentActors />}
                        >__Quick partner display__</Button>
                    </Link>
                </Grid>
            </Grid>

            <Modal
                className={styles.modal}
                open={isPartnerTasksModalOpen}
                onClose={() => setPartnerTasksModalOpen(false)}
                aria-labelledby="view-partner-tasks-modal"
                aria-describedby={translations.modalWindowDescription}
            >
                <Paper className={styles.modalPaper}>
                    <Typography align="center">{translations.modalPartnerIdInputField}</Typography>
                    <TextField 
                        data-test-id="partner-id-modal-input"
                        aria-label="partner id modal input"
                        value={partnerId} 
                        size="small"
                        onChange={e => setPartnerId(Number(e.target.value))} 
                        variant="outlined"
                        type="number"
                    />
                    <Link to={DEMOAPP_URLS.partnerDetails.replace(":partnerId", partnerId.toString())}>
                        <Button
                            data-test-id="show-partner-details-modal-button"
                            aria-label="show partner details modal button"
                            className={styles.modalButton} 
                            size="small"
                            variant="contained"
                        >
                            {translations.modalShowPartnerDetailsButton}
                        </Button>
                    </Link>
                </Paper>
            </Modal>
        </>
    );
};

export default MainPage;
