import { makeStyles, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
        },
        modalPaper: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "stretch",
            outline: "none",
            width: "22rem",
        },
        modalButton: {
            float: "right",
            marginTop: "1rem"
        },
        header: {
            margin: "4rem 0"
        }
    })
);

const useMenuButtonStyles = makeStyles((theme: Theme) => ({
    root: {
        width: "20rem",
        height: "15rem"
    },
    label: {
        flexDirection: "column",
        justifyContent: "center"
    },
    startIcon: {
        margin: "0 0 0.5rem 0",
        "& > *": {
            width: "7rem",
            height: "7rem"
        }
    }
}));

export {
    useMenuButtonStyles
};

export default useStyles;
