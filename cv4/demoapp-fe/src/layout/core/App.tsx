import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import AppStyled from "./App.styled";
import AppHeader from "./AppHeader/AppHeader";
import AppFooter from "./AppFooter/AppFooter";
import LoginViewPage from "./LoginViewPage/LoginViewPage";
import AppHomePage from "./AppHomePage/AppHomePage";

import { Providers, useUser } from "@core";
import { APP_BASENAME, APP_URLS } from "@constants";
import appRouters from "../appRouters";

const App = () => {
  
  const user = useUser();
   
  return (
    <Providers>
      <AppStyled>
        <Router basename={APP_BASENAME}>
          <AppHeader />

          <section className="app-section">
            <Switch>

              {user.redirectToLogin && <Redirect to={APP_URLS.login} />}

              <Route exact path={APP_URLS.login}>
                <LoginViewPage/>
              </Route>

              {Object.values(appRouters).map((subAppRoutes: any, index: number) =>
                Object.keys(subAppRoutes).map((routeKey: any) => {
                  
                  const route =  subAppRoutes[routeKey];

                  return (
                    <Route
                      key={`sub-app-${index}-${routeKey}`}
                      path={route.path} 
                      component={route.component} 
                    />
                  );
                })
              )}

              <Route exact path={APP_URLS.home}>
                <AppHomePage/>
              </Route>
              <Redirect to={APP_URLS.home} />

            </Switch>
          </section>

          <AppFooter/>
        </Router>

      </AppStyled>
    </Providers>
  );
};

export default App;
