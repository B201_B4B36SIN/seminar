import styled from "styled-components";
import { colors } from "@theme";

const AppHeaderStyled = styled.header`

    display: flex;
    align-items: center;
    flex-direction: column;
    background-color: ${colors.app.title.background};
    padding: 1rem;

    .language-switch {
        width: 100%;
        max-width: 76.8rem;
        height: 2rem;
    }
    
    .header-bar {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        width: 100%;
        max-width: 76.8rem;

        @media only screen and (max-width: 768px) {
            padding: 0 0.5rem;
        }

        &--title {
            margin-right: auto;
            display: inline;
            font-size: 4rem;
            justify-self: flex-start;
            text-transform: uppercase;
            color: ${colors.text.secondary};
            cursor: pointer;

            @media only screen and (max-width: 768px) {
                font-size: 2rem;
            }
        }

        &--user-info {
            justify-content: space-between;
            color: ${colors.text.secondary};
            margin-right: 1rem;
        }
    }

    
`;

export default AppHeaderStyled;
