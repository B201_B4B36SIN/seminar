import React, { useState } from "react";
import { Popper, ClickAwayListener, List, Paper, ListItemIcon, ListItemText, ListItem, IconButton } from "@material-ui/core";
import { ExitToApp, Menu } from "@material-ui/icons";
import { useUser } from "@core";
import { usePopperStyles } from "./styles";

const UserMenu = () => {
    
    const user = useUser();
    const [anchorEl, setAnchorEl] = useState<HTMLElement>(null);

    const signOut = () => {
        setAnchorEl(null);
    };

    const handleMenuClick = (e: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(anchorEl ? null : e.currentTarget);
    }

    return (
        <>
            <IconButton 
                aria-describedby="menu-button"
                data-test-id="menu-popup"
                size="small" 
                color="secondary"
                onClick={handleMenuClick}
            >
                <Menu />
            </IconButton>

            <Popper 
                id="menu-button" 
                open={!!anchorEl} 
                anchorEl={anchorEl}
                placement="left-start"
            >
                <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
                    <Paper classes={usePopperStyles()}>
                        <List>
                            {user.isLoggedIn && (
                                <>
                                    <ListItem 
                                        button 
                                        aria-label="user-sign-out"
                                        data-test-id="user-sign-out"
                                        onClick={signOut}
                                    >
                                        <ListItemIcon><ExitToApp /></ListItemIcon>
                                        <ListItemText primary="Sign out" />
                                    </ListItem>
                                </>
                            )}
                        </List>
                    </Paper>
                </ClickAwayListener>
            </Popper>
        </>
    );
};

export default UserMenu;
