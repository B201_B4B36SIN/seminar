import React from "react";
import { Link } from "react-router-dom";
import { LanguageContext, useUser } from "@core";
import { LanguageSelect } from "@components";
import { useLocalization } from "@localization";
import { APP_URLS } from "@constants";
import UserMenu from "./UserMenu/UserMenu";
import AppHeaderStyled from "./AppHeader.styled";
import transFile from "./translations.json";

const AppHeader = () => {

    const user = useUser();
    const translations = useLocalization(transFile);

    return (
        <AppHeaderStyled>
            <div className="language-switch">
                <LanguageContext.Consumer>
                    {({ language, setLanguage }) => (
                        <LanguageSelect language={language} setLanguage={setLanguage}/>
                    )}
                </LanguageContext.Consumer>
            </div>
            <div className="header-bar">
                <Link 
                    className="header-bar--title" 
                    to={APP_URLS.home}
                >{translations.appHeader}</Link> 
                {user.isLoggedIn && (
                    <span className="header-bar--user-info">
                        {user.name} {user.surname}
                    </span>
                )}

                <UserMenu />

            </div>
            
        </AppHeaderStyled>
    );
};

export default AppHeader;
