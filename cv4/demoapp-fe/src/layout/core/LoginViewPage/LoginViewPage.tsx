import React, { useState } from "react";
import { Button } from "@components";
import { TextField, Paper } from "@material-ui/core";
import { usePaperStyles, useTextFieldStyles, useBottomBarStyles } from "./styles";
import langFile from "./translations.json";
import { useLocalization } from "@localization";

const LoginViewPage = () => {

    const translations = useLocalization(langFile);
    const [loginID, setLoginID] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
    const fieldStyles = useTextFieldStyles();

    const clearLoginFields = () => {
        setLoginID("");
        setLoginPassword("");
    };

    const signIn = () => {
        clearLoginFields();
    };

    return (
        <>
            <Paper classes={usePaperStyles()}>
                <TextField 
                    aria-label="Login ID"
                    data-test-id="login-id"
                    id="login-id" 
                    label={translations.inputUsername}
                    variant="outlined"
                    size="small"
                    value={loginID}
                    onChange={e => setLoginID(e.target.value)}
                    classes={fieldStyles}
                />
                <TextField
                    aria-label="Login password"
                    data-test-id="login-password"
                    id="login-password"
                    label={translations.inputPassword}
                    variant="outlined"
                    size="small"
                    type="password"
                    value={loginPassword}
                    onChange={e => setLoginPassword(e.target.value)}
                    classes={fieldStyles}
                />
                <p>(test test to log in)</p>
            </Paper>
            <Paper elevation={0} classes={useBottomBarStyles()}>
                <Button
                    onClick={clearLoginFields}
                    size="small"
                    data-test-id="clear"
                >{translations.clearButton}</Button>
                <Button
                    variant="contained"
                    data-test-id="sign-in"
                    size="small"
                    onClick={signIn}
                >{translations.loginButton}</Button>
            </Paper>
        </>
    )
};

export default LoginViewPage;
