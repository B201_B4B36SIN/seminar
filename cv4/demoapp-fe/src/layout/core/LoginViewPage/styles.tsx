import { makeStyles, Theme } from "@material-ui/core";

const usePaperStyles = makeStyles((theme: Theme) => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    }
}));

const useTextFieldStyles = makeStyles((theme: Theme) => ({
    root: {
        maxWidth: "25rem",
        
        "&:first-child": {
            paddingBottom: "1rem"
        }
    }
}));

const useBottomBarStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: "2rem",
        background: "transparent",
        "& :last-child": {
            float: "right"
        }
    }
}));

export { 
    usePaperStyles,
    useTextFieldStyles,
    useBottomBarStyles
};
