import React from "react";
import { Typography } from "@material-ui/core";
import transFile from "./translations.json";
import { useLocalization } from "@localization";

const AppFooter = () => {

    const translations = useLocalization(transFile);

    return (
        <footer className="app-footer">
          <Typography color="primary" align="center">
            {translations.footerVersion} 0.1.1
          </Typography> 
        </footer>
    )
};

export default AppFooter;
