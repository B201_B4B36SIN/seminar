import ComponentsLibraryRouter from "./componentsLibrary";
import DemoappRouter from "./demoapp";

const appRouters = {
    componentLibrary: ComponentsLibraryRouter,
    demoapp: DemoappRouter
};

export default appRouters;
