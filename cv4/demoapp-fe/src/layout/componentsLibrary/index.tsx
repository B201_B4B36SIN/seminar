import MainPage from "./MainPage/MainPage";
import { COMPONENTS_LIBRARY_URLS } from "@constants";

const routes = {
    main: {
        path: COMPONENTS_LIBRARY_URLS.main,
        component: MainPage
    }
};

export default routes;

