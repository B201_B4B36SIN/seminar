import React from "react";
import { Button } from "@components";
import { Table, TableBody, TableRow, TableCell, Typography, Paper } from "@material-ui/core";
import useStyles from "./styles";

const MainPage = () => {

    const styles = useStyles();

    return (
        <>
            <Paper>
                <Typography variant="h3" align="center" color="primary">Buttons</Typography>
                <Table>
                    <TableBody>
                        <TableRow>
                            <TableCell className={styles.cell}>
                                <Button variant="contained">Primary contained</Button>
                            </TableCell>
                           <TableCell className={styles.cell}>
                                <Button variant="contained" size="small">Primary contained small</Button>
                            </TableCell>
                        </TableRow>
                        
                        <TableRow >
                           <TableCell className={styles.cell}>
                                <Button variant="outlined">Primary outlined</Button>
                            </TableCell>
                           <TableCell className={styles.cell}>
                                <Button variant="outlined" size="small">Primary outlined small</Button>
                            </TableCell>
                        </TableRow>

                        <TableRow>
                           <TableCell className={styles.cell}>
                                <Button variant="text">Primary text</Button>
                            </TableCell>
                           <TableCell className={styles.cell}>
                                <Button variant="text" size="small">Primary text small</Button>
                            </TableCell>
                        </TableRow>

                    </TableBody>
                   
                </Table>
            </Paper>
        </>
    );
}

export default MainPage;
