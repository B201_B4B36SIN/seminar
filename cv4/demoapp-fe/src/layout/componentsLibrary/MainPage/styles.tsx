import { makeStyles, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        cell: {
            textAlignLast: "center"
        }
    }
));


export default useStyles;
