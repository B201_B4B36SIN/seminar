import * as actions from "../../actionsLibrary";
import { StoreAction, PartnerDataItem } from "@library";

const initialState: PartnerDataItem[] = [];

const partnersList = (state = initialState, action: StoreAction<PartnerDataItem[]>) => {
    switch (action.type) {
      case actions.SET_PARTNERS_LIST:
        return action.payload;
      default:
        return state;
    }
};

export default partnersList;
