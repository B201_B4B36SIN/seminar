import { combineReducers } from "redux";
import partnersRootReducer from "./partners";

const rootReducer = combineReducers({
    partners: partnersRootReducer
});

export default rootReducer;