const APP_BASENAME = "/dap-demoapp";
const LOCAL_STORAGE_LOCALIZATION_KEY = "lang";

const APP_URLS = {
	login: "/login",
	home: "/home"
};

const demoappBase = "/partners";
const DEMOAPP_URLS = {
	basepath: demoappBase,
	main: `${demoappBase}/main`,
	partnersList: `${demoappBase}/search-for-partners`,
	partnerDetails: `${demoappBase}/:partnerId/details`
};

const componentsLibBasePath = "/components-lib";
const COMPONENTS_LIBRARY_URLS = {
	basepath: `${componentsLibBasePath}`,
	main: `${componentsLibBasePath}/main`
};

export {
	APP_BASENAME,
	LOCAL_STORAGE_LOCALIZATION_KEY,
	APP_URLS,
	DEMOAPP_URLS,
	COMPONENTS_LIBRARY_URLS
};
