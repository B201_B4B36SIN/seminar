import axios from "axios";
import { DEMOAPP_BASE_URL, TOKEN } from "./constants";
import { PartnerTaskItem } from "@library";

const updateTaskState = async (partnerId: number, taskId: number, setTaskToActive: boolean) => axios.post(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/tasks/${taskId}`, 
    { status: { code: setTaskToActive ? "ACTIVE" : "COMPLETED" }},
    { headers: { Authorization: TOKEN } }
);

const deleteCompletedTasks = async (partnerId: number) => axios.post(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/delete-completed-tasks`, 
    {},
    { headers: { Authorization: TOKEN } }
);

const getTasks = async (partnerId: number) => axios.get<PartnerTaskItem[]>(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/tasks`, 
    { headers: { Authorization: TOKEN } }
);

const createNewTask = async (partnerId: number, newTaskText: string) => axios.post<number>(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/tasks`, 
    { text: newTaskText },
    { headers: { Authorization: TOKEN } }
);

const deleteTask = async (partnerId: number, taskId: number) => axios.delete(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/tasks/${taskId}`,
    { headers: { Authorization: TOKEN } }
)

const updateTaskName = async (partnerId: number, taskId: number, newPartnerText: string) => axios.put(
    `${DEMOAPP_BASE_URL}/partners/${partnerId}/tasks/${taskId}`, 
    { text: newPartnerText },
    { headers: { Authorization: TOKEN } }
);

export {
    getTasks,
    updateTaskState,
    deleteCompletedTasks,
    deleteTask,
    createNewTask,
    updateTaskName
};
    