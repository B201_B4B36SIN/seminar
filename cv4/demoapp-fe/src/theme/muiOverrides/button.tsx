import colors from "../colors";

const containedPrimary = colors.button.contained.primary;
const containedSecondary = colors.button.contained.secondary;
const text = colors.button.text;
const outlined = colors.button.outlined;

export const button = {
	root: {
		padding: "2.2rem 2.4rem",
		borderRadius: "0.4rem",
		"&:hover": {
			transition: "all 100ms ease-in-out",
        },
        underline: "none"
    },
    
    sizeSmall: {
        padding: "1.2rem",
        fontSize: "1.4rem"
    },

	contained: {
        color: containedPrimary.default.text,
        background: `linear-gradient(155deg, ${containedPrimary.default.background.from} 0%, ${containedPrimary.default.background.to} 100%)`,
        border: "none",
        boxShadow: `0px 0.0034rem 0.0053rem rgba(242, 79, 0, 0.0590406), 
            0px 0.008rem 0.0126rem rgba(242, 79, 0, 0.0848175), 
            0px 0.015rem 0.23rem rgba(242, 79, 0, 0.105), 
            0px 0.27rem 0.42rem rgba(242, 79, 0, 0.125183), 
            0px 0.5rem 0.8rem rgba(242, 79, 0, 0.150959), 
            0px 1.2rem 1.9rem rgba(242, 79, 0, 0.21);`,
        "&:hover": {
            border: "none",
            background: `linear-gradient(155deg, ${containedPrimary.hover.background.from} 0%, ${containedPrimary.hover.background.to} 100%)`,
            boxShadow: `0px 0.470434px 0.608796px rgba(242, 79, 0, 0.0759093), 
                0px 1.13052px 1.46302px rgba(242, 79, 0, 0.109051), 
                0px 2.12866px 2.75474px rgba(242, 79, 0, 0.16), 
                0px 3.79717px 4.91399px rgba(242, 79, 0, 0.18), 
                0px 7.10219px 9.19107px rgba(242, 79, 0, 0.24), 
                0px 17px 22px rgba(242, 79, 0, 0.3);`
        },
        "&:active": {
            boxShadow: `0px 0.11069px 
                0.138363px rgba(242, 79, 0, 0.101212),
                0px 0.266004px 0.332505px rgba(242, 79, 0, 0.145401), 
                0px 0.500862px 0.626078px rgba(242, 79, 0, 0.18), 
                0px 0.893452px 1.11682px rgba(242, 79, 0, 0.214599), 
                0px 1.6711px 2.08888px rgba(242, 79, 0, 0.258788), 
                0px 4px 5px rgba(242, 79, 0, 0.36);`
        },
        "&:disabled": {
            background: containedPrimary.disabled.background
        }
    },
    containedSecondary: {
        background: containedSecondary.background
    },
    text: {
		padding: "2.2rem 2.4rem",
        color: text.default.text,
        background: text.default.background,
        border: "none",
        "&:hover": {
            border: "none",
            background: text.hover.background,
        },
        "&:disabled": {
            border: text.disabled.border,
            color: text.disabled.text,
            background: text.disabled.background,
        },
    },
    outlined: {
		padding: "2.2rem 2.4rem",
        color: outlined.default.text,
        background: outlined.default.background,
        border: `0.04rem solid ${outlined.default.border}`,
        "&:hover": {
            background: outlined.hover.bacgkround,
            border: `0.04rem solid ${outlined.hover.border}`,
        },
        "&:active": {
            background: outlined.active.background
        },
        "&:disabled": {
            border: outlined.disabled.border,
            color: outlined.disabled.text,
            background: outlined.disabled.background,
        }
    },
};

export const iconButton = {
    root: {
        padding: "0.5rem"
    }
};
