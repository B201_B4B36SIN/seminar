import colors from "../colors";

export const row = {
    root: {
        borderBottom: `1px solid ${colors.table.row.border}`,
        "&:last-child": {
            border: "none"
        }
    },
	head: {
        lineHeight: "2.4rem"
    }
};

export const cell = {
    root: {
        borderBottom: "none"
    },
	head: {
        lineHeight: "2.4rem"
    }
};
