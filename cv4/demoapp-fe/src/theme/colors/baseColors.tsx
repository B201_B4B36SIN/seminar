const baseColors = {
    transparent: "rgba(0, 0, 0, 0)", 

    red: "#E50000",
    lightRed: "#FF6161",
    whiteRed: "#FFEDED",
    coolGrey: "#737373",
 
    orange: "#F24F00",
    orange12: "rgba(242, 79, 0, 0.12)",
    orange36: "rgba(242, 79, 0, 0.36)",
    lightOrange: "#F86F04",  
    paleOrange: "#FFEBD6",
    
    limeGreen: "#27AA5E",
    paleLimeGreen: "#C7FFCC",
    
    veryDarkBlue: "#1E1E40",
    darkBlue: "#2C3464",
    moderateBlue: "#4B52A6",
    moderateBlue20: "rgba(75, 82, 166, 20)",

    white: "#fff",
    grey5: "#F4F4F4",
    grey4: "#D4D4D4",
    grey3: "#959595 ",
    grey2: "#383838",
    grey1: "#303030",
    black: "#000",
    black40: "rgba(0, 0, 0, 0.4)",
    black60: "rgba(0, 0, 0, 0.6)",
    black80: "rgba(0, 0, 0, 0.8)",
};

export default baseColors;
