import { makeStyles, createStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
        },
        progress: {
            outline: "none",
            "&:focus": {
                outline: "none",
            }
        }
    })
);

export default useStyles;
