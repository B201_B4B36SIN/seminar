import React, { useState, useEffect } from "react";
import { CEZTableProps, CEZTableHeadItem } from "@library";
import { Table, Typography, TableBody, TableCell, TableRow, TableHead, TableSortLabel } from "@material-ui/core";
import { stableSort, getComparator } from "./helpers";
import { useHeaderTableIconStyles } from "./styles";

const CEZTable = ({ titles, items, classes, emptyTableMessage, mapItemsToTitleIds }: CEZTableProps) => {
    
    const [bodyItems, setBodyItems] = useState<any[]>();
    const [paginatedItems, setPaginatedItems] = useState<any[]>();
    const [orderByItemId, setOrderMasterItemId] = useState<string>();
    const [orderDirection, setOrderDirection] = useState<"asc" | "desc">();
    const headerTableIconStyles = useHeaderTableIconStyles();

    const handleSortClick = (itemId: string) => {
        if (orderByItemId === itemId) {
            setOrderDirection(orderDirection === "asc" ? "desc" : "asc");
        }
        else {
            setOrderDirection("asc");
            setOrderMasterItemId(itemId);
        }

        const comparator = getComparator(orderDirection, orderByItemId);
        const newItems: any[] = stableSort(bodyItems, comparator);
        const paginatedItems: any[] = newItems.slice(0, 10);

        setPaginatedItems(paginatedItems);
    };

    useEffect(() => {

        if (items) {

            const newBodyItems = Object.keys(items).map((itemKey: any) => {
                const originalItem = items[itemKey];
                
                if (mapItemsToTitleIds) {

                    let mappedItem: any = {};

                    titles.forEach((title: CEZTableHeadItem) => {
                        mappedItem[title.id as any] = originalItem[title.id];
                    });
                    
                    return mappedItem;
                }
                else return originalItem;
            });
            
            setBodyItems(newBodyItems);
            setPaginatedItems(newBodyItems.slice(0, 10));
        }

    }, [items, mapItemsToTitleIds, titles]);

    return (
        <>
        {(!bodyItems && emptyTableMessage) ? <Typography align="center">{emptyTableMessage}</Typography> : (
            <Table aria-label="simple-table" classes={classes}>
                <TableHead>
                    <TableRow>
                        {titles && titles.map((item: CEZTableHeadItem) => (
                            <TableCell
                                key={item.id}
                                align={item.numeric ? 'right' : 'left'}
                                padding={item.disablePadding ? 'none' : 'default'}
                                sortDirection={orderByItemId === item.id ? orderDirection : false}
                            >
                                {!item.unsortable ? (
                                    <TableSortLabel
                                        classes={headerTableIconStyles}
                                        active={orderByItemId === item.id}
                                        direction={orderByItemId === item.id ? orderDirection : 'asc'}
                                        onClick={() => handleSortClick(item.id)}
                                    >
                                        {item.value}
                                        {orderDirection === item.id ? (
                                            // <span className={classes.visuallyHidden}>
                                            <span>
                                                {orderDirection === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                            </span>
                                        ) : null}
                                    </TableSortLabel> 
                                ) : item.value }
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {paginatedItems.map((item: any, columnIdex: number) => (
                        <TableRow key={`cez-list-column-${columnIdex}`}>
                            {Object.keys(item).map((itemKey: string, rowIndex: number) => (
                                <TableCell 
                                    key={`cez-list-row-${rowIndex}`}
                                    component="th" 
                                    scope="row"
                                >
                                    {item[itemKey]}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        )}
        </>
    );
};

export default CEZTable;
