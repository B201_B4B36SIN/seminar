import { makeStyles, Theme } from "@material-ui/core";

const useHeaderTableIconStyles = makeStyles((theme: Theme) => ({
    icon: {
        height: "2rem",
        width: "2rem"
    }
}));

export {
    useHeaderTableIconStyles
};
