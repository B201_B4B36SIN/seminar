import React, { useState } from "react";
import { Button } from "@components";
import { ELocalizationLanguage, LanguageSelectProps } from "@library";
import { ClickAwayListener, List, ListItem, ListItemIcon, ListItemText, Divider, Popper, Paper } from "@material-ui/core";
import { Language } from "@material-ui/icons";
import { usePopperStyles, useLangButtonStyles,useLangButtonIconStyles } from "./styles";

const languages = [
    "CZ",
    "ENGB"
];

const LanguageSelect = ({ language, setLanguage }: LanguageSelectProps) => {

    const [anchorEl, setAnchorEl] = useState<HTMLElement>();

    const handleLanguageChange = (e:any, key: any) => {
        setAnchorEl(null);
        setLanguage(key as ELocalizationLanguage);
    };

    const getSelectableLanguagesList = () => {
        return languages.map(key => {
            if (key === language) return null;
            return (
                <ListItem 
                    button 
                    aria-label={`change language to ${key}`}
                    data-test-id={`language-selector-${key}`}
                    onClick={e => handleLanguageChange(e, key)}
                    key={`language-select-${key}`}
                >
                    <ListItemIcon><Language /></ListItemIcon>
                    <ListItemText primary={key} />
                </ListItem>
            );
        })
    }
    
    const button = (
        <Button
            color="secondary"
            aria-label="toggle-language-options"
            data-test-id="toggle-language-options"
            classes={useLangButtonStyles()}
            onClick={(e: any) => setAnchorEl(anchorEl ? null : e.target)}
            endIcon={<Language classes={useLangButtonIconStyles()} />}
        >
            {language}
        </Button>
    );

    return (
        <>
            {button}
            <Popper 
                id="language-menu-button" 
                open={!!anchorEl} 
                anchorEl={anchorEl}
                placement="left-start"
            >
                <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
                    <Paper classes={usePopperStyles()}>
                        <List>
                            <ListItem 
                                aria-label={`current language: ${language}`}
                                data-test-id={`current language: ${language}`}
                            >
                                <ListItemIcon><Language /></ListItemIcon>
                                <ListItemText primary={language} />
                            </ListItem>
                            <Divider />
                            {getSelectableLanguagesList()}                                        
                        </List>
                    </Paper>
                </ClickAwayListener>
            </Popper>
        </>
    );
};

export default LanguageSelect;
