import { makeStyles, Theme } from "@material-ui/core";

const useContainerStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 0,
        margin: "1rem",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between"
    }
}));

const useHeaderStyles = makeStyles((theme: Theme) => ({
    root: {
        width: "100%",
        paddingBottom: "1rem"
    }
}));

export {
    useContainerStyles,
    useHeaderStyles
};
