Application {{ .Values.name | upper | quote }} on {{ .Values.name }}-{{ .Values.env }} is running on URL:
internal: "{{- if .Values.ingress.enableTls -}}https{{- else -}}http{{- end -}}://{{ .Values.env }}.{{ .Values.ingress.mainDomain }}/{{ tpl .Values.ingress.rewriteTarget . }}"
{{- if .Values.Config.PublicUrl }}, external: "{{- if .Values.ingress.enableTls -}}https{{- else -}}http{{- end -}}://{{ .Values.Config.PublicUrl }}/{{ .Values.name }}"{{- end }}
