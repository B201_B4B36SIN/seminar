package com.example.rest;

/**
 * Created by Jirka on 21.4.2020.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
public class ProductServiceController {
    private static Map<String, Product> productRepo = new HashMap<>();

    static {
        Product honey = new Product();
        honey.setId("1");
        honey.setName("Honey");
        productRepo.put(honey.getId(), honey);
        Product almond = new Product();
        almond.setId("2");
        almond.setName("Almond");
        productRepo.put(almond.getId(), almond);
    }

    @RequestMapping(value = "/products")
    public ResponseEntity<Object> getProduct() {
        System.out.println("COntroller");
        return new ResponseEntity<>(productRepo.values(), HttpStatus.OK);
    }

    /*
1. @PathParam
 /employees/{emp_id}

get id napriklad.. kdzy neajdu tak 404
2. @QueryParam

cokoli jinyho filtrovani prijmeni apod..
/employees?firstname=jmeno


3. @RequestBody

POST PUT, cely objekty, vytvareni a nebo uprava




 kody status

200 OK
3xx presmerovani... Oauth2
4xx chyba
5xx internal server error



     */
}