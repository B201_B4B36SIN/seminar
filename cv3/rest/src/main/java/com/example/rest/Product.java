package com.example.rest;

/**
 * Created by Jirka on 21.4.2020.
 */
public class Product {
    private String id;
    private String name;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}