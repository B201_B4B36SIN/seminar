package cz.cez.framework.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TEST_PARTNER")
public class TestPartner implements Serializable {

    @Id
    @Column(name = "PARTNER_KEY")
    private int partnerKey;

    @Column(name = "DEL_FLAG")
    private int delFlag;

    @Column(name = "INS_PROCESS_ID")
    private int insProcessId;

    @Column(name = "INS_DT")
    private String insDt;

    @Column(name = "UPD_PROCESS_ID")
    private int updProcessId;

    @Column(name = "UPD_DT")
    private String updDt;

    @Column(name = "UPD_EFF_DT")
    private String updEffDt;

    @Column(name = "SRC_UPD_DT")
    private String srcUpdDt;

    @Column(name = "SRC_ID")
    private int srcId;

    @Column(name = "SRC_SYS_ID")
    private String srcSysId;

    @Column(name = "TP_PARTNER_KEY")
    private int tpPartnerKey;

    @Column(name = "COMP_NAME")
    private String compName;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "BRTH_DATE", columnDefinition = "DATE")
    private String brthDate;

    @Column(name = "DEATH_DATE")
    private String deathDate;


    public TestPartner() {
    }

    public int getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(int partnerKey) {
        this.partnerKey = partnerKey;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public int getInsProcessId() {
        return insProcessId;
    }

    public void setInsProcessId(int insProcessId) {
        this.insProcessId = insProcessId;
    }

    public String getInsDt() {
        return insDt;
    }

    public void setInsDt(String insDt) {
        this.insDt = insDt;
    }

    public int getUpdProcessId() {
        return updProcessId;
    }

    public void setUpdProcessId(int updProcessId) {
        this.updProcessId = updProcessId;
    }

    public String getUpdDt() {
        return updDt;
    }

    public void setUpdDt(String updDt) {
        this.updDt = updDt;
    }

    public String getUpdEffDt() {
        return updEffDt;
    }

    public void setUpdEffDt(String updEffDt) {
        this.updEffDt = updEffDt;
    }

    public String getSrcUpdDt() {
        return srcUpdDt;
    }

    public void setSrcUpdDt(String srcUpdDt) {
        this.srcUpdDt = srcUpdDt;
    }

    public int getSrcId() {
        return srcId;
    }

    public void setSrcId(int srcId) {
        this.srcId = srcId;
    }

    public String getSrcSysId() {
        return srcSysId;
    }

    public void setSrcSysId(String srcSysId) {
        this.srcSysId = srcSysId;
    }

    public int getTpPartnerKey() {
        return tpPartnerKey;
    }

    public void setTpPartnerKey(int tpPartnerKey) {
        this.tpPartnerKey = tpPartnerKey;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBrthDate() {
        return brthDate;
    }

    public void setBrthDate(String brthDate) {
        this.brthDate = brthDate;
    }

    public String getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = deathDate;
    }
}