package cz.cez.framework;

import cz.cez.framework.model.TpTestPartner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TpTestPartnerRestController {
    @Autowired
    private TpTestPartnerRepository tpTestPartnerRepository;
    @Autowired
    private TpTestPartnerService tpTestPartnerService;

    public void setTestPartnerRepository(TestPartnerRepository testPartnerRepository, TpTestPartnerService tpTestPartnerService) {
        this.tpTestPartnerRepository = tpTestPartnerRepository;
        this.tpTestPartnerService = tpTestPartnerService;
    }

    @GetMapping("/api/echo")
    public String getEcho() {
        //sleep 3s
        return "It works!!";
    }

    @GetMapping("/api/tpTestPartner")
    public List<TpTestPartner> getTTPs() {
        List<TpTestPartner> testPartners = tpTestPartnerRepository.findAll();
        return testPartners;
    }

    @GetMapping("/api/tpTestPartner/{id}")
    public Optional<TpTestPartner> getTTP(@PathVariable(name="id")Integer id) {
        return tpTestPartnerRepository.findById(id);
    }

    @PostMapping("/api/tpTestPartner")
    public ResponseEntity<?> saveTTP(@RequestBody TpTestPartner tpTestPartner){
        tpTestPartnerService.insertWithQuery(tpTestPartner);
        return ResponseEntity.ok("resource saved");
    }

    @DeleteMapping("/api/tpTestPartner/{id}")
    public ResponseEntity<?> deleteTTP(@PathVariable(name="id")Integer id){
        tpTestPartnerRepository.deleteById(id);
        return ResponseEntity.ok("resource deleted");
    }

    @PutMapping("/api/tpTestPartner/{id}")
    public ResponseEntity<?> updateTTP(@RequestBody TpTestPartner tpTestPartner,
                                      @PathVariable(name="id")Integer id){
        TpTestPartner ttp = tpTestPartnerRepository.getOne(id);
        if(ttp != null){
            tpTestPartnerService.updateWithQuery(tpTestPartner, id);
        }
        return ResponseEntity.ok("resource updated");
    }

}