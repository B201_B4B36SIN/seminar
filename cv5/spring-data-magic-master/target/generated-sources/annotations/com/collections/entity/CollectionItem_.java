package com.collections.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CollectionItem.class)
public abstract class CollectionItem_ {

	public static volatile SingularAttribute<CollectionItem, String> summary;
	public static volatile SingularAttribute<CollectionItem, Image> image;
	public static volatile SingularAttribute<CollectionItem, String> country;
	public static volatile SingularAttribute<CollectionItem, Image> smallImage;
	public static volatile SingularAttribute<CollectionItem, Short> year;
	public static volatile SingularAttribute<CollectionItem, BigDecimal> price;
	public static volatile SetAttribute<CollectionItem, String> topics;
	public static volatile SingularAttribute<CollectionItem, String> name;
	public static volatile SingularAttribute<CollectionItem, String> description;
	public static volatile SingularAttribute<CollectionItem, Long> id;

	public static final String SUMMARY = "summary";
	public static final String IMAGE = "image";
	public static final String COUNTRY = "country";
	public static final String SMALL_IMAGE = "smallImage";
	public static final String YEAR = "year";
	public static final String PRICE = "price";
	public static final String TOPICS = "topics";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

